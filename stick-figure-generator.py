from __future__ import annotations

import json
import math
import pathlib
import random
import sys
from subprocess import Popen

import cairosvg
import numpy as np


BLOOD_COLOR = (0, 0, 255, 255)
STICK_FIGURE_COLOR = (0, 0, 0, 255)


def make_random_direction() -> complex:
    """Returns a random complex number representing a direction. Its distance from the origin is 1."""
    return math.e ** (1j * math.tau * random.random())


def make_new_position_at_a_radius(old_position: complex, distance: float) -> complex:
    """Returns a random position that is `distance` away from the `old_position`."""
    return old_position + distance * make_random_direction()


def make_random_stick_figure_pose() -> list[tuple[float, float]]:
    """Returns a list of stick figure limb positions.

    The root coordinate, the head, is in the interval [0, 1). The rest of the
    coordinates may be a bit under or above 0 and 1.
    """

    head_neck_distance = 0.05
    elbow_neck_distance = arm_tip_elbow_distance = 0.1
    pelvis_neck_distance = 0.15
    knee_pelvis_distance = 0.1
    leg_tip_knee_distance = 0.1

    head_position = random.random() + random.random() * 1j

    neck_position = make_new_position_at_a_radius(head_position, head_neck_distance)

    left_elbow_position = make_new_position_at_a_radius(
        neck_position, elbow_neck_distance
    )
    right_elbow_position = make_new_position_at_a_radius(
        neck_position, elbow_neck_distance
    )

    left_arm_tip_position = make_new_position_at_a_radius(
        left_elbow_position, arm_tip_elbow_distance
    )
    right_arm_tip_position = make_new_position_at_a_radius(
        right_elbow_position, arm_tip_elbow_distance
    )

    pelvis_position = make_new_position_at_a_radius(neck_position, pelvis_neck_distance)

    left_knee_position = make_new_position_at_a_radius(
        pelvis_position, knee_pelvis_distance
    )
    right_knee_position = make_new_position_at_a_radius(
        pelvis_position, knee_pelvis_distance
    )

    left_leg_top_position = make_new_position_at_a_radius(
        left_knee_position, leg_tip_knee_distance
    )
    right_leg_top_position = make_new_position_at_a_radius(
        right_knee_position, leg_tip_knee_distance
    )

    return [
        (x.real, x.imag)
        for x in (
            head_position,
            neck_position,
            left_elbow_position,
            right_elbow_position,
            left_arm_tip_position,
            right_arm_tip_position,
            pelvis_position,
            left_knee_position,
            right_knee_position,
            left_leg_top_position,
            right_leg_top_position,
        )
    ]


def make_stick_figure_svg(
    stick_figure_pose: list[tuple[float, float]], image_width: int, image_height: int
) -> str:
    svg = f'<svg version="1.1" width="{image_width}" height="{image_height}" xmlns="http://www.w3.org/2000/svg">'

    # Avoid having a stick figure on a transparent background by first drawing
    # a white rectangle all over our image.
    svg += f"""
    <rect width="100%" height="100%" fill="white" />
"""

    svg += f"""
    <circle cx="{stick_figure_pose[0][0]}"
            cy="{stick_figure_pose[0][1]}"
            r="{15*image_width/500}"
            fill="rgba{STICK_FIGURE_COLOR}" />
"""

    # The pairs of integers here are the coordinates between which we want to
    # draw a line - the limbs of our stick figure.
    for index_a, index_b in [
        (0, 1),
        (1, 2),
        (1, 3),
        (2, 4),
        (3, 5),
        (1, 6),
        (7, 6),
        (8, 6),
        (7, 9),
        (8, 10),
    ]:
        svg += f"""
    <line x1="{stick_figure_pose[index_a][0]}"
          y1="{stick_figure_pose[index_a][1]}"
          x2="{stick_figure_pose[index_b][0]}"
          y2="{stick_figure_pose[index_b][1]}"
          stroke="rgba{STICK_FIGURE_COLOR}"
          stroke-width="{2*image_width/500}" />
"""

    return svg + "</svg>"


def lerp(t: float, a: float, b: float) -> float:
    return (b - a) * t + a


def main() -> None:
    try:
        number_of_stick_figures = int(sys.argv[1])
    except Exception:
        print(f"Usage: {sys.argv[0]} [number of stick figures]")
        sys.exit(-1)

    random.seed(0)

    image_width, image_height = 200, 200

    dataset_path = pathlib.Path() / "stick-figure-pose-dataset"
    dataset_images_path = dataset_path / "images"

    if not dataset_path.exists():
        dataset_path.mkdir()

    if not dataset_images_path.exists():
        dataset_images_path.mkdir()

    stick_figure_poses = []

    for stick_figure_pose_index in range(number_of_stick_figures):
        print(stick_figure_pose_index)

        # Abstract math space limb locations.
        stick_figure_pose_in_abstract_space = make_random_stick_figure_pose()

        # Convert the abstract math space into image space.
        # Just realised the stick figures would be stretched if the image width
        # and heights are not the same size.
        stick_figure_pose_in_image_space = [
            (
                lerp(coordinate[0], 0, image_width),
                lerp(coordinate[1], 0, image_height),
            )
            for coordinate in stick_figure_pose_in_abstract_space
        ]

        stick_figure_poses.append(
            {
                "stick-figure-index": stick_figure_pose_index,
                "stick-figure-pose": stick_figure_pose_in_image_space,
            }
        )

        svg_source = make_stick_figure_svg(
            stick_figure_pose_in_image_space,
            image_width=image_width,
            image_height=image_height,
        )

        with open(
            f"{dataset_images_path}/stick_figure_{stick_figure_pose_index:05d}.svg", "w"
        ) as svg_file:
            svg_file.write(svg_source)

        cairosvg.svg2png(
            svg_source,
            write_to=f"{dataset_images_path}/stick_figure_{stick_figure_pose_index:05d}.png",
        )

    with open(dataset_path / "stick_figure_poses.json", "w") as f:
        json.dump(stick_figure_poses, f)


if __name__ == "__main__":
    main()
