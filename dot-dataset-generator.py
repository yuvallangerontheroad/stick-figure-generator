import math
import pathlib
import json
import random
import sys
from subprocess import Popen

import cairosvg
import numpy as np


BACKGROUND_COLOR = (0, 0, 0, 255)
DOT_COLOR = (0, 0, 0, 255)
IMAGE_WIDTH, IMAGE_HEIGHT = 200, 200


def make_random_dot_pose(width, height):
    return random.uniform(0, width), random.uniform(0, height)


def make_dot_svg(dot_pose, image_width=200, image_height=200):
    svg = f"""
<svg version="1.1"
     width="{image_width}"
     height="{image_height}"
     xmlns="http://www.w3.org/2000/svg">
"""

    # create blank image
    svg += f"""
    <rect width="100%" height="100%" fill="white" />
"""

    svg += f"""
    <circle cx="{dot_pose[0]}"
            cy="{dot_pose[1]}"
            r="{10*image_width/500}"
            fill="rgba{DOT_COLOR}" />
"""

    return svg + "</svg>"


def main():
    try:
        number_of_images = int(sys.argv[1])
    except Exception:
        print(f"Usage: {sys.argv[0]} [number of images]")
        sys.exit(-1)

    random.seed(0)

    dataset_path = pathlib.Path() / "dot-dataset"
    dataset_images_path = dataset_path / "images"

    if not dataset_path.exists():
        dataset_path.mkdir()

    if not dataset_images_path.exists():
        dataset_images_path.mkdir()

    dot_data = []

    for dot_index in range(number_of_images):
        dot_pose_image = make_random_dot_pose(IMAGE_WIDTH, IMAGE_HEIGHT)

        print(dot_index)

        svg_source = make_dot_svg(dot_pose_image)

        with open(dataset_images_path / f"dot-{dot_index:05}.svg", "w") as svg_file:
            svg_file.write(svg_source)

        cairosvg.svg2png(
            svg_source,
            write_to=str(dataset_images_path / f"dot-{dot_index:05}.png"),
        )

        dot_data.append(
            {
                "dot-index": dot_index,
                "dot-pose-in-image": dot_pose_image,
            }
        )

    with open(dataset_path / "dot-data.json", "w") as f:
        json.dump(dot_data, f)


if __name__ == "__main__":
    main()
